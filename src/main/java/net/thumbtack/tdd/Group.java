package net.thumbtack.tdd;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Group {
    private final AuthService authService;
    private final List<User> users;

    private String name;

    public Group(AuthService authService, String name) {
        this.authService = authService;
        this.name = name;
        this.users = new ArrayList<User>();
    }

    public User createUser(String name) {
        User user = new User(name);
        users.add(user);
        return user;
    }

    public String getName() {
        return name;
    }

    public Group createSubGroup(String subGroupName) {
        return new Group(authService, subGroupName);
    }

    public List<User> getUsers() {
        return users;
    }

    public List<User> getAuthUsers() {
        List<User> authUsers = new ArrayList<User>();
        for (User user: users) {
            /*if (user.getName().equalsIgnoreCase("admin")) {
                authUsers.add(user);
            } else */
            if (authService.isAuthorized(user)) {
                authUsers.add(user);
            }
        }
        return authUsers;
    }
}
