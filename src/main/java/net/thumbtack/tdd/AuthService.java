package net.thumbtack.tdd;

/**
 *
 */
public interface AuthService {
    boolean isAuthorized(User user);
}
