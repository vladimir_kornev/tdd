package net.thumbtack.tdd;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 *
 */
@Test
@RunWith(Parameterized.class)
public class TestApplication {
    private final AuthService authService = Mockito.mock(AuthService.class);


    @DataProvider()
    public Object[][] users() {
        return new Object[][]{
                {"a"},

                {null},

                {"aasdasdasdadasdasdasdadasdasda"}
        };
    }

    @DataProvider()
    public Object[][] groups() {
        return new Object[][]{
                {"a"},

                {null},

                {"aasdasdasdadasdasdasdadasdasda"}
        };
    }

    public void testCreateGroup() {
        Group group = new Group(authService, "gname");
        assertNotNull(group);
        assertEquals(group.getName(), "gname");
    }

    @Test(dataProvider = "users")
    public void testCreateUser(String userName, int i) {
        Group group = new Group(authService, "gname");
        User user = group.createUser(userName);
        assertNotNull(user);
        assertEquals(user.getName(), userName);
    }

    @Test(dataProvider = "groups")
    public void testCreateSubGroup() {
        Group group = new Group(authService, "gname");
        Group subGroup = group.createSubGroup("sgname");
        assertEquals(subGroup.getName(), "sgname");
        assertFalse(group == subGroup);
    }

    public void testListUsers() {
        Group group = new Group(authService, "group");
        User u1 = group.createUser("user1");
        group.createUser("user2");
        List<User> users = group.getUsers();
        assertNotNull(users);
        assertEquals(users.size(), 2);
    }

    public void testThatGroupReturnAuthorizedUsers() {
        AuthService stub = Mockito.mock(AuthService.class);

        Group group = new Group(stub, "gorup");
        User user1 = group.createUser("true");
        User user2 = group.createUser("name");
        when(stub.isAuthorized(user1)).thenReturn(true);
        when(stub.isAuthorized(user2)).thenReturn(false);

        assertEquals(group.getAuthUsers().size(), 1);
    }

    public void testThatGroupReturnAuthorizedUsersBasedOnCommunicationWithExternalServer() {
        AuthService mock = Mockito.mock(AuthService.class);

        Group group = new Group(mock, "group");
        User user1 = group.createUser("admin");
        User user2 = group.createUser("name");
        Mockito.when(mock.isAuthorized(user1)).thenReturn(true);
        when(mock.isAuthorized(user2)).thenReturn(false);

        List<User> authUsers = group.getAuthUsers();
        assertTrue(authUsers.size() == 1);

        //Assert.assertTrue(mock.isAuthorized(user1));
        //Assert.assertFalse(mock.isAuthorized(user2));

        Mockito.verify(mock, times(1)).isAuthorized(user1);
        Mockito.verify(mock, times(1)).isAuthorized(user2);
    }
}
