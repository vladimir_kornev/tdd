package net.thumbtack.tdd;

/**
 *
 */
public class AuthServiceStub implements AuthService {

    public boolean isAuthorized(User user) {
        if ("true".equalsIgnoreCase(user.getName())) {
            return true;
        }
        return false;
    }
}
